For the book "data structures and algorithms with javascript"
Since the accompanying code has some errors and does not include all, I cloned the repo and  added my own files.

if you use the "console.lgo" to print out the info, you can use "node script.js" to run it.
if you use the "print" to print out the info, you can use "js script.js" to run it, or simply press "F5" in my vim.

For an array, console.log will give out the array, while print will give out a string, since print calls the to_string
method automatically.

